FROM docker.elastic.co/logstash/logstash-oss:6.3.2
RUN rm -f /usr/share/logstash/pipeline/logstash.conf
COPY log4j2.properties /usr/share/logstash/config/log4j2.properties
COPY java/java.security /usr/lib/jvm/java-1.8.0-openjdk-1.8.0.161-0.b14.el7_4.x86_64/jre/lib/security/java.security
ADD pipeline/ /usr/share/logstash/pipeline/
